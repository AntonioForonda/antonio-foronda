using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly ILogger<EmployeesController> _logger;

        public EmployeesController(ILogger<EmployeesController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "Employees")]
        public IEnumerable<employee> Get()

        {
            return MyEmployees.all;
        }

        [HttpGet("Cashiers")]
        public IEnumerable<employee> GetCashiers()
        {
            return MyEmployees.cashiers;
        }
        [HttpGet("Security")]
        public IEnumerable<employee> GetSecurityStaff()
        {
            return MyEmployees.securityGuards;
        }
        [HttpGet("Chefs")]
        public IEnumerable<employee> GetChefs()
        {
            return MyEmployees.chefs;
        }
        [HttpGet("{id}")]
        public IEnumerable<employee> GetById(int id)
        {
            List<employee> filt = new List<employee>();
            try
            {
                foreach(var emp in MyEmployees.all)
                {
                    if (emp.id == id)
                    {
                        filt.Add(emp);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception();
            }
            return filt;
        }
    }
}