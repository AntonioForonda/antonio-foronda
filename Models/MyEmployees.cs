﻿namespace WebApplication1.Models
{
    public class MyEmployees
    {

        internal static Address genericAddress = new Address("123 Main St.", "New York", "NY", 60647);

        internal static employee cashier1 =new employee(11, "Laura", "Gutierrez", Gender.Female, DateTime.Now, new DateTime(1987, 09, 22), 17, genericAddress);
        internal static employee cashier2 = new employee(12, "Andres", "Chavez", Gender.Male, DateTime.Now, new DateTime(1998, 08, 12), 12, genericAddress);
        internal static employee cashier3 = new employee(13, "Adrian", "Santos", Gender.Male, DateTime.Now, new DateTime(1968, 03, 08), 28, genericAddress);

        internal static employee security1 = new employee(21, "Ryan", "Lopez", Gender.Male, DateTime.Now, new DateTime(1993, 11, 02), 15, genericAddress);
        internal static employee security2 = new employee(22, "Sergio", "Gonsalez", Gender.Male, DateTime.Now, new DateTime(1989, 07, 14), 18, genericAddress);
        internal static employee security3 = new employee(23, "Sarah", "Gomez", Gender.Female, DateTime.Now, new DateTime(2001, 05, 07), 11, genericAddress);

        internal static employee chef1 = new employee(31, "Patrick", "Chavez", Gender.Male, DateTime.Now, new DateTime(1973, 08, 06), 21, genericAddress);
        internal static employee chef2 = new employee(32, "Dana", "Rivero", Gender.Female, DateTime.Now, new DateTime(1995, 09, 29), 18, genericAddress);
        internal static employee chef3 = new employee(33, "Gabriela", "Calle", Gender.Female, DateTime.Now, new DateTime(1997, 01, 03), 15, genericAddress);

        internal static List<employee> cashiers = new List<employee>
        {
            cashier1, cashier2, cashier3
        };
        internal static List<employee> securityGuards = new List<employee>
        {
            security1, security2, security3
        };
        internal static List<employee> chefs = new List<employee>
        {
            chef1, chef2, chef3
        };
        internal static List<employee> all = new List<employee>
        {
            cashier1, cashier2, cashier3, security1, security2, security3, chef1, chef2, chef3
        };
       
    }
}
