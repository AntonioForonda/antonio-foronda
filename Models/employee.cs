﻿namespace WebApplication1.Models
{

    public class employee
    {
        public int id { get; set; }
        public string? firstName { get; set; }
        public string? lastName { get; set; }
        public Gender gender { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime birthDate { get; set; }
        public decimal wage { get; set; }
        public Address? address { get; set; }
        public employee (int id, string? firstName, string? lastName, Gender gender, DateTime createdDate, DateTime birthDate, decimal wage, Address address)
        {
            this.id = id;       
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
            this.createdDate = createdDate;
            this.birthDate = birthDate;
            this.wage = wage;
            this.address = address;
        }



    }
}
